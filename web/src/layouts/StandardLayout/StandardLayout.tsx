import {
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  Button,
  Drawer,
  TextField,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Divider,
  ButtonGroup,
} from '@mui/material'
import MenuIcon from '@mui/icons-material/Menu'
import { useState } from 'react'
import { Article, RemoveCircle } from '@mui/icons-material'
import { Beleg } from 'src/pages/ToolPage/ToolPage'
import { useLoaderContext } from 'src/context/LoaderContext'
import { generatePDF } from 'src/pages/ToolPage/GeneratePDF'
import { generateAllJSON } from 'src/pages/ToolPage/GenerateJSONOutput'

type StandardLayoutProps = {
  children?: React.ReactNode
}

const StandardLayout = ({ children }: StandardLayoutProps) => {
  const [drawer, setDrawer] = useState<boolean>(false)
  const [name, setName] = useState(
    sessionStorage.getItem('name') || ''
  )
  if (!localStorage.getItem('belege'))
    localStorage.setItem('belege', JSON.stringify({}))
  const [belege, setBelege] = useState<{ [key: string]: Beleg }>(
    JSON.parse(localStorage.getItem('belege'))
  )
  const loaderContext = useLoaderContext()

  return (
    <>
      <AppBar position="static" className="AppBar">
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
            onClick={() => setDrawer(true)}
          >
            <MenuIcon />
          </IconButton>
          <Drawer
            anchor={'left'}
            open={drawer}
            onClose={() => setDrawer(false)}
          >
            <List>
              {Object.keys(belege).length ? (
                Object.keys(belege).map((belegKey) => (
                  <ListItem key={belegKey} disablePadding>
                    <ListItemButton
                      onClick={() => {
                        loaderContext.setBelegToLoad((current) => ({
                          ...current,
                          belegToLoad: belegKey,
                        }))
                        sessionStorage.setItem("name", belege[belegKey].name)
                        setName(()=>belege[belegKey].name)
                      }}
                    >
                      <ListItemIcon>
                        <Article />
                      </ListItemIcon>
                      <ListItemText primary={belegKey} />
                    </ListItemButton>
                    <ListItemIcon>
                      <IconButton
                        size="medium"
                        edge="end"
                        aria-label="delete beleg"
                        onClick={() => {
                          const copy = { ...belege }
                          delete copy[belegKey]
                          localStorage.setItem('belege', JSON.stringify(copy))
                          setBelege(JSON.parse(localStorage.getItem('belege')))
                        }}
                      >
                        <RemoveCircle />
                      </IconButton>
                    </ListItemIcon>
                  </ListItem>
                ))
              ) : (
                <ListItem>
                  <Typography variant="body1">
                    Keine Belege gespeichert...
                  </Typography>
                </ListItem>
              )}
              <Divider />
              <ButtonGroup
                sx={{ ml: 13, mr: 13, mt: 3 }}
                orientation={'vertical'}
                variant="contained"
                aria-label="outlined primary button group"
              >
                <Button onClick={()=>generatePDF(null, true)}>Alle als PDF</Button>
                <Button
                  onClick={() => {
                    localStorage.removeItem('belege')
                    localStorage.removeItem("file")
                    setBelege({})
                  }}
                >
                  Alle löschen
                </Button>
                <Button onClick={()=>generateAllJSON()}>Alle als JSON</Button>
              </ButtonGroup>
            </List>
          </Drawer>

          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            Rechnungsinator 2.0
          </Typography>
          <TextField
            variant="filled"
            sx={{
              mt: 1,
              mb: 1,
              mr: 2,
              input: { color: 'white' },
              label: { color: 'white' },
            }}
            label={'Name'}
            className={'nameInput'}
            value={name}
            onChange={(e) => {
              if (e.target.value.includes('Rick'))
                open('https://youtu.be/dQw4w9WgXcQ')
              setName(()=>e.target.value)
              sessionStorage.setItem('name', e.target.value)
            }}
          ></TextField>
          <Button color="inherit" onClick={()=>{
            loaderContext.setBelegToLoad((current)=>({...current, belegToLoad: "clear"}))
          }}>Neu</Button>
        </Toolbar>
      </AppBar>
      {children}
    </>
  )
}

export default StandardLayout
