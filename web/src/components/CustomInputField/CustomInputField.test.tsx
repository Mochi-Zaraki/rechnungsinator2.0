import { render } from '@redwoodjs/testing/web'

import CustomInputField from './CustomInputField'

//   Improve this test with help from the Redwood Testing Doc:
//    https://redwoodjs.com/docs/testing#testing-components

describe('CustomInputField', () => {
  it('renders successfully', () => {
    expect(() => {
      render(<CustomInputField />)
    }).not.toThrow()
  })
})
