import { Button, Grid, TextField, Typography } from "@mui/material"
import React from "react"

enum Variant {
  OUTLINED = "outlined",
  FILLED = "filled",
  STANDARD =  "standard"
}

interface CustomInputFieldState {
  inputFields: Array<React.ReactNode>
}

class CustomInputField extends React.Component {
  id: string
  label: string
  variant: Variant
  declare state: Readonly<CustomInputFieldState>
  constructor(props: any) {
    super(props)
    this.id = props.id
    this.label = props.label
    this.variant = props.kind
    this.state = {
      inputFields: [<Grid key={this.state.inputFields.length} item><TextField id={this.id} label={this.label} variant={this.variant}/></Grid>]
    }
  }

  newMember(): void {
    this.setState({
      inputFields: this.state.inputFields.push(<Grid key={this.state.inputFields.length} item><TextField id={this.id} label={this.label} variant={this.variant} /></Grid>)
    })
    console.log(this.state)
  }

  render(): any {
    return (
      <Grid direction={"column"} rowSpacing={5} container item lg={4}>
      <Grid item><Typography variant='h5' noWrap component={"div"}>Wer?</Typography></Grid>
      {this.state.inputFields}
      <Grid item><Button variant='contained' onClick={()=>this.newMember()}>mehr</Button></Grid>
    </Grid>
    )
  }
}



export default CustomInputField