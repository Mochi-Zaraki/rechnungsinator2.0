import { FatalErrorBoundary, RedwoodProvider } from '@redwoodjs/web'
import { RedwoodApolloProvider } from '@redwoodjs/web/apollo'
import React from 'react'
import FatalErrorPage from 'src/pages/FatalErrorPage'

import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import "dayjs/locale/de"

import Routes from 'src/Routes'

import './index.css'
import { LocalizationProvider } from '@mui/x-date-pickers';
import { LoaderContextProvider } from './context/LoaderContext';

const App = () => (
  <React.StrictMode>
    <LoaderContextProvider>
    <LocalizationProvider dateAdapter={AdapterDayjs} adapterLocale={"de"}>
  <FatalErrorBoundary page={FatalErrorPage}>
    <RedwoodProvider titleTemplate="%PageTitle | %AppTitle">
      <RedwoodApolloProvider>
        <Routes />
      </RedwoodApolloProvider>
    </RedwoodProvider>
  </FatalErrorBoundary>
  </LocalizationProvider>
  </LoaderContextProvider>
  </React.StrictMode>
)

export default App
