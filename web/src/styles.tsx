import { makeStyles,createStyles, Theme, ClassNameMap } from "@mui/material"

interface JSS {
    [className: string]: {
        [prop:string]: string | number
    }
}

const styles: JSS = {
    appbar: {
        margin: "0 0 14px 0",
        bgColor: "black"
    }
}

export type StyleClasses = () => typeof styles

export const useStyles: ()=>ClassNameMap = makeStyles((theme: Theme)=> createStyles(styles))