import { createContext, Dispatch, SetStateAction, useContext, useState } from "react";

export type LoaderContext = {
    belegToLoad: string,
    setBelegToLoad: Dispatch<SetStateAction<LoaderContext>>
}

// const initState = {
//     belegToLoad: "",
//     setBelegToLoad: ()=>{}
// }

class LoaderContextInit implements LoaderContext {
    belegToLoad: string;
    constructor() {
        this.belegToLoad = ""
    }
    setBelegToLoad: Dispatch<SetStateAction<LoaderContext>>;
}

export const loaderContext = createContext<LoaderContext>(new LoaderContextInit())

export const LoaderContextProvider = ({children}): JSX.Element => {
    const [belegToLoad, setBelegToLoad] = useState(new LoaderContextInit())

    return(
        <loaderContext.Provider value={{...belegToLoad, setBelegToLoad}}>
            {children}
        </loaderContext.Provider>
    )
} 

export const useLoaderContext = ()=>useContext(loaderContext)