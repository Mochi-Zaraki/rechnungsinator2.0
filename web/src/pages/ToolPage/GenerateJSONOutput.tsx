import { Beleg } from "./ToolPage";



export function generateJSON(exportObj: Beleg){
    const dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(exportObj));
    const downloadAnchorNode = document.createElement('a');
    downloadAnchorNode.setAttribute("href",     dataStr);
    downloadAnchorNode.setAttribute("download", `Belegabrechnung_${exportObj.name}_${new Date(
        exportObj.date
      ).toLocaleDateString('de-DE', {
        minute: '2-digit',
        hour: '2-digit',
      })}.json`);
    document.body.appendChild(downloadAnchorNode); // required for firefox
    downloadAnchorNode.click();
    downloadAnchorNode.remove();
}

export function generateAllJSON() {
  const dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(localStorage.getItem("belege"));
  const downloadAnchorNode = document.createElement('a');
  downloadAnchorNode.setAttribute("href",     dataStr);
  downloadAnchorNode.setAttribute("download", `Belegabrechnung_Sammlung_${new Date().toLocaleDateString('de-DE', {
      minute: '2-digit',
      hour: '2-digit',
    })}.json`);
  document.body.appendChild(downloadAnchorNode); // required for firefox
  downloadAnchorNode.click();
  downloadAnchorNode.remove();
}