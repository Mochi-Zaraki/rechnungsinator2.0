import type { ComponentMeta } from '@storybook/react'

import ToolPage from './ToolPage'

export const generated = () => {
  return <ToolPage />
}

export default {
  title: 'Pages/ToolPage',
  component: ToolPage,
} as ComponentMeta<typeof ToolPage>
