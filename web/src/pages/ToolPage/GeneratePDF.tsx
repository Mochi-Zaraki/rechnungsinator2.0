import jsPDF from 'jspdf'
import autoTable from 'jspdf-autotable'
import { Beleg, BetragValue } from './ToolPage'

const floatToPercent = (f: number) => {
  return String(f).replace('0.', '') + '%'
}

export function generatePDF(beleg?: Beleg, many = false) {
  console.log('PDF RUN')
  const doc = new jsPDF({ compress: true, unit: 'mm' })
  if (many) {
    var belege: { [key: string]: Beleg } = JSON.parse(
      localStorage.getItem('belege')
    )
    for (let belegName in belege) {
      belege[belegName].betrag = belege[belegName].betrag.map(
        (betrag) => new BetragValue(betrag.brutto, betrag.steuer)
      )
      belege[belegName].files = [
        JSON.parse(localStorage.getItem('file'))[belegName],
      ]
    }
  } else {
    var belege: { [key: string]: Beleg } = { default: beleg }
  }
  const tableHeaders = [
    ['BelegNr', 'Beschreibung', 'Datum', 'Betrag', 'MwSt-Satz', 'Netto'],
  ]
  const tableData = []
  Object.keys(belege).forEach((belegName, j) => {
    for (let i = 0; i < belege[belegName].betrag.length; i++) {
      const row = []
      if (i == 0) {
        row.push({
          rowSpan: belege[belegName].betrag.length,
          content: j+1,
        })
        row.push({
          rowSpan: belege[belegName].betrag.length,
          content: belege[belegName].beschreibung,
        })
        row.push({
          rowSpan: belege[belegName].betrag.length,
          content: new Date(belege[belegName].date).toLocaleDateString(),
        })
      }
      row.push(belege[belegName].betrag[i].brutto + '€')
      row.push(floatToPercent(belege[belegName].betrag[i].steuer))
      row.push(belege[belegName].betrag[i].netto() + '€')
      tableData.push(row)
    }
  })

  let summe = 0
  for (let belegName in belege) {
    belege[belegName].betrag.forEach(
      (currentBetrag) => (summe += Number(currentBetrag.netto()))
    )
  }

  doc.text('Belegabrechnung', 20, 10)
  doc.text('Name: ' + sessionStorage.getItem('name'), 20, 20)
  doc.text('Datum: ' + new Date().toLocaleDateString(), 20, 30)
  doc.text('Summe: ' + summe.toFixed(2) + '€', 20, 40)

  autoTable(doc, {
    head: tableHeaders,
    body: tableData,
    includeHiddenHtml: true,
    styles: { halign: 'left', valign: 'top', cellWidth: 'wrap' },
    headStyles: { lineWidth: { left: 0.25, right: 0.25 }, lineColor: 255 },
    columnStyles: { 1: { cellWidth: 'auto', overflow: 'linebreak' } },
    margin: { bottom: 50 },
    pageBreak: 'avoid',
    startY: 50,
  })
  autoTable(doc, {
    head: [['Teilnehmer']],
    body: Object.keys(belege).map((belegName) =>
      belege[belegName].members.map((member) => [member])
    ),
    styles: { halign: 'left' },
    margin: { bottom: 50, top: 50 },
  })

  if (beleg?.files.length || many)
    Object.keys(belege).forEach((belegName, i) => {
      const file = belege[belegName].files[0]
      const image = new Image()
      image.onload = () => {
        const max = {
          height: doc.internal.pageSize.getHeight(),
          width: doc.internal.pageSize.getWidth(),
        }

        let height = image.height,
          width = image.width,
          ratio = image.height / image.width
        if (height > max.height || width > max.width) {
          if (height > width) {
            height = max.height
            width = height * (1 / ratio)
            // Making reciprocal of ratio because ration of height as width is no valid here needs width as height
          } else if (width > height) {
            width = max.width
            height = width * ratio
            // Ratio is valid here
          }
        }

        doc.addPage()
        // doc.setPage(i + 2)
        doc.addImage(image, 'jpg', 0, 0, width, height)
        doc.text(
          'Belegnummer: ' + belege[belegName].belegNummer,
          max.width / 2,
          max.height - 15
        )
        if (Object.keys(belege).length === i+1)
          doc.save(
            `Belegabrechnung_${sessionStorage.getItem('name')}_${
              !many
                ? new Date(beleg.date).toLocaleDateString('de-DE', {
                    minute: '2-digit',
                    hour: '2-digit',
                  })
                : 'Sammlung' + new Date().toLocaleDateString()
            }.pdf`
          )
      }
      if (typeof file === 'string') image.src = file
      else image.src = URL.createObjectURL(file)
    })
  else {
    if (confirm('PDF ohne Bild speichern?'))
      doc.save(
        `BelegabrechnungOhneBild_${beleg.name}_${new Date(
          beleg.date
        ).toLocaleDateString('de-DE', {
          minute: '2-digit',
          hour: '2-digit',
        })}.pdf`
      )
    else return
  }
}