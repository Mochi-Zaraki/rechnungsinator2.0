import { MetaTags } from '@redwoodjs/web'
import {
  Button,
  Container,
  Grid,
  IconButton,
  InputAdornment,
  MenuItem,
  Select,
  SelectChangeEvent,
  TextField,
  Typography,
} from '@mui/material'
import {
  BrowserUpdatedOutlined,
  FileUploadOutlined,
  PictureAsPdf,
  RemoveCircleOutline,
} from '@mui/icons-material/'
import { generatePDF } from './GeneratePDF'
import { useLayoutEffect, useState } from 'react'
import { DatePicker } from '@mui/x-date-pickers'
import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date'
import { useLoaderContext } from 'src/context/LoaderContext'

import type {} from '@mui/x-date-pickers/themeAugmentation'
import dayjs from 'dayjs'
import { generateJSON } from './GenerateJSONOutput'

export interface Betrag {
  brutto: number | string
  steuer: number
  netto: () => string
}

export interface Beleg {
  members: Array<string>
  betrag: Array<Betrag>
  beschreibung: string
  date: string
  files: Array<File> | Array<string>
  name: string
  belegNummer: number
}

enum Save {
  BROWSER = 'browser',
  JSON = 'json',
  PDF = 'pdf',
}

export class BetragValue implements Betrag {
  brutto: number | string
  steuer: number
  constructor(brutto?, steuer?) {
    ;(this.brutto = brutto || 1), (this.steuer = steuer || 0)
  }
  netto(): string {
    return (Number(this.brutto) - Number(this.brutto) * this.steuer).toFixed(
      2
    )
  }
}

const ToolPage: React.FC = () => {
  const currentDate = new Date().toLocaleDateString('de-DE', {
    minute: '2-digit',
    hour: '2-digit',
  })
  const loaderContext = useLoaderContext()

  const saveAsDataURL = (file, belegName: string) => {
    const storedFiles = JSON.parse(localStorage.getItem('file')) || {}
    if (typeof file !== 'string') {
      const reader = new FileReader()
      reader.onload = (e) => {
        localStorage.setItem(
          'file',
          JSON.stringify({
            ...storedFiles,
            [belegName + ' ' + currentDate]: e.target.result,
          })
        )
      }
      reader.readAsDataURL(file)
    } else
      localStorage.setItem(
        'file',
        JSON.stringify({
          ...storedFiles,
          [belegName + ' ' + currentDate]: file,
        })
      )
  }

  const loadFiles = ()=> {
    const base64: string = JSON.parse(localStorage.getItem("file"))[loaderContext.belegToLoad]
    return [base64]
  }

  const [members, setMembers] = useState<Array<string>>([''])
  const [Belegnummer, setBelegnummer] = useState(
    Object.keys(JSON.parse(localStorage.getItem('belege'))).length + 1
  )
  const [date, setDate] = useState<MaterialUiPickersDate | null>(
    dayjs(new Date())
  )
  const [beschreibung, setBeschreibung] = useState('')

  const [betrag, setBetrag] = useState<Array<Betrag | null>>([
    new BetragValue(),
  ])
  const [files, setFiles] = useState<Array<File> | Array<string>>([])

  const loadHandle = () => {
    const loadedBeleg: Beleg = JSON.parse(localStorage.getItem('belege'))[
      loaderContext.belegToLoad
    ]
    console.log('loader: ', loaderContext.belegToLoad)
    setFiles(() => loadFiles())
    setMembers(() => [...loadedBeleg.members])
    setBelegnummer(() => loadedBeleg.belegNummer)
    setBeschreibung(() => loadedBeleg.beschreibung)
    setDate(() => dayjs(loadedBeleg.date))
    setBetrag(() =>
      loadedBeleg.betrag.map(
        (betrag) => new BetragValue(betrag.brutto, betrag.steuer)
      )
    )
  }
  const clear = () => {
    setBelegnummer(
      () => Object.keys(JSON.parse(localStorage.getItem('belege'))).length + 1
    )
    setMembers(() => [''])
    setBeschreibung(() => '')
    setDate(() => dayjs(new Date()))
    setBetrag(() => [new BetragValue()])
    setFiles(() => [])
  }

  useLayoutEffect(() => {
    if (loaderContext.belegToLoad && loaderContext.belegToLoad !== 'clear')
      loadHandle()
    if (loaderContext.belegToLoad === 'clear') clear()
  }, [loaderContext.belegToLoad])

  const removeBetrag = (dindex: number) => {
    setBetrag((current) => {
      const copyBetrag = [...current]
      copyBetrag.splice(dindex, 1)
      return copyBetrag
    })
  }

  const removeMember = (dindex: number) => {
    setMembers((current) => {
      const copyMembers = [...current]
      copyMembers.splice(dindex, 1)
      return copyMembers
    })
  }

  const handleFileUpload = (e: React.ChangeEvent<HTMLInputElement>) => {
    const fileList: FileList = e.target.files
    const fileArray = Array.from(fileList)
    setFiles(() => fileArray)
  }

  const save = (kind: Save) => {
    setBelegnummer((current)=>current+1)
    // const saveBetrag = betrag.filter((val) => val !== null)
    // const saveMembers = members.filter((val) => val !== null)
    const beleg: Beleg = {
      members,
      betrag,
      beschreibung: beschreibung || 'siehe unten',
      date: date.toISOString(),
      files,
      name:
        sessionStorage.getItem('name') || 'Default_Name_',
      belegNummer: Belegnummer,
    }
    switch (kind) {
      case Save.BROWSER:
        if (!localStorage.getItem('belege'))
          localStorage.setItem('belege', JSON.stringify({}))
        const belege: { [key: string]: typeof beleg } = JSON.parse(
          localStorage.getItem('belege')
        )
        belege[beleg.name + ' ' + currentDate] = beleg
        saveAsDataURL(beleg.files[0], beleg.name)
        localStorage.setItem('belege', JSON.stringify(belege))
        setBelegnummer(Object.keys(belege).length)
        break
      case Save.JSON:
        generateJSON(beleg)
        break
      case Save.PDF:
        generatePDF(beleg)
        break
      default:
        throw new Error('Could not save. Error in switch statement (kind)')
    }
  }

  return (
    <>
      <MetaTags title="Rechnungsinator 2.0" description="Belegabrechnungen generieren leicht gemacht (vom Schülerpraktikanten^^)" />

      <Container disableGutters className="MainConatiner">
        <Grid
          container
          columnSpacing={2}
          wrap={'nowrap'}
          className="rootGrid"
          justifyContent={'center'}
          columns={8}
        >
          <Grid
            direction={'column'}
            rowSpacing={5}
            alignItems={'center'}
            container
            item
            lg={2}
          >
            <Grid item>
              <Typography variant="h5" noWrap gutterBottom component={'div'}>
                Nummer
              </Typography>
            </Grid>
            <Grid item>
              <Typography variant="subtitle1" align="center" component={'div'}>
                {Belegnummer}
              </Typography>
            </Grid>
          </Grid>
          <Grid
            direction={'column'}
            rowSpacing={5}
            alignItems={'center'}
            container
            item
            lg={4}
          >
            <Grid item>
              <Typography variant="h5" noWrap component={'div'}>
                Wer?
              </Typography>
            </Grid>

            {members.map((member, i) => {
              if (member === null) return false
              else
                return (
                  <Grid key={i} item>
                    <TextField
                      className={'Teilnehmer'}
                      label={'Neuer Teilnehmer'}
                      variant={'outlined'}
                      required
                      value={member}
                      InputLabelProps={{ shrink: true }}
                      InputProps={{
                        endAdornment: (
                          <InputAdornment position="end">
                            <IconButton
                              aria-label="delete member"
                              onClick={() => removeMember(i)}
                              edge="end"
                            >
                              <RemoveCircleOutline />
                            </IconButton>
                          </InputAdornment>
                        ),
                      }}
                      onChange={(e) => {
                        setMembers((current) => {
                          const copy = [...current]
                          copy[i] = e.target.value
                          return copy
                        })
                      }}
                    />
                  </Grid>
                )
            })}
            <Grid item>
              <Button
                variant="contained"
                onClick={() => {
                  setMembers((current) => [
                    ...current,
                    '', //This is the new name
                  ])
                }}
              >
                mehr
              </Button>
            </Grid>
          </Grid>
          <Grid
            direction={'column'}
            rowSpacing={5}
            alignItems={'center'}
            container
            item
            lg={4}
          >
            <Grid item>
              <Typography variant="h5" noWrap component={'div'}>
                Was?
              </Typography>
            </Grid>
            <Grid item>
              <Grid item>
                <TextField
                  className={'Teilnehmer'}
                  label={'Beschreibung'}
                  variant={'outlined'}
                  multiline
                  InputLabelProps={{ shrink: true }}
                  value={beschreibung}
                  helperText={'Was habt ihr gemacht?'}
                  onChange={(e) => {
                    setBeschreibung(() => e.target.value)
                  }}
                />
              </Grid>
            </Grid>
          </Grid>
          <Grid
            direction={'column'}
            rowSpacing={5}
            alignItems={'center'}
            container
            item
            lg={4}
          >
            <Grid item>
              <Typography variant="h5" noWrap component={'div'}>
                Wann?
              </Typography>
            </Grid>
            <Grid item>
              <DatePicker
                renderInput={(params) => (
                  <TextField helperText="Wann ist es passiert?" {...params} />
                )}
                value={date}
                onChange={(newDate: MaterialUiPickersDate) => setDate(newDate)}
                label={'Wähle ein Datum'}
              />
            </Grid>
          </Grid>
          <Grid
            direction={'column'}
            rowSpacing={5}
            alignItems={'center'}
            container
            item
            lg={4}
          >
            <Grid item>
              <Typography variant="h5" noWrap component={'div'}>
                Brutto
              </Typography>
            </Grid>

            {betrag.map((currentBetrag, i) => {
              if (currentBetrag === null) return false
              else
                return (
                  <Grid key={i} item>
                    <TextField
                      id={'brutto' + i}
                      label="Brutto Betrag"
                      variant="outlined"
                      value={currentBetrag.brutto}
                      type={'number'}
                      inputProps={{ inputMode: 'decimal', pattern: '[0-9]*' }}
                      InputProps={{
                        endAdornment: (
                          <InputAdornment position="end">
                            <IconButton
                              aria-label="delete betrag"
                              onClick={() => removeBetrag(i)}
                              edge="end"
                            >
                              <RemoveCircleOutline />
                            </IconButton>
                          </InputAdornment>
                        ),
                      }}
                      onChange={(e) => {
                        setBetrag((current) => {
                          const copy = [...current]
                          copy[i].brutto = e.target.value
                          return copy
                        })
                      }}
                    />
                  </Grid>
                )
            })}
            <Grid item>
              <Button
                variant="contained"
                onClick={() => {
                  setBetrag((current) => [...current, new BetragValue()])
                }}
              >
                mehr
              </Button>
            </Grid>
          </Grid>
          <Grid
            direction={'column'}
            rowSpacing={5}
            alignItems={'center'}
            container
            item
            lg={3}
          >
            <Grid item>
              <Typography variant="h5" noWrap component={'div'}>
                Steuer
              </Typography>
            </Grid>

            {betrag.map((currentBetrag, i) => {
              if (currentBetrag === null) return false
              else
                return (
                  <Grid key={i} item>
                    <Select
                      id={'steuer' + i}
                      value={currentBetrag.steuer}
                      onChange={(e: SelectChangeEvent<number>) => {
                        setBetrag((current) => {
                          const copy = [...current]
                          copy[i].steuer = Number(e.target.value)
                          return copy
                        })
                      }}
                    >
                      <MenuItem value={0}>0%</MenuItem>
                      <MenuItem value={0.05}>5%</MenuItem>
                      <MenuItem value={0.07}>7%</MenuItem>
                      <MenuItem value={0.16}>16%</MenuItem>
                      <MenuItem value={0.19}>19%</MenuItem>
                    </Select>
                  </Grid>
                )
            })}
          </Grid>
          <Grid
            direction={'column'}
            rowSpacing={5}
            alignItems={'center'}
            container
            item
            lg={2}
          >
            <Grid item>
              <Typography variant="h5" noWrap component={'div'}>
                Netto
              </Typography>
            </Grid>

            {betrag.map((currentBetrag, i) => {
              if (currentBetrag === null) return false
              else
                return (
                  <Grid key={i} item>
                    <Typography
                      variant="subtitle1"
                      component={'div'}
                      sx={{
                        mt: 1.75,
                        mb: 1.75,
                      }}
                    >
                      {currentBetrag.netto()}
                    </Typography>
                  </Grid>
                )
            })}
          </Grid>
          <Grid
            direction={'column'}
            rowSpacing={5}
            alignItems={'center'}
            container
            item
            lg={2}
          >
            <Grid item>
              <Typography variant="h5" noWrap component={'div'}>
                Rechnung
              </Typography>
            </Grid>
            <Grid item>
              <Button variant="outlined" component={'label'}>
                <FileUploadOutlined />
                <Typography sx={{ ml: '4px' }} noWrap>
                  Rechnung hochladen
                </Typography>
                <input
                  type={'file'}
                  hidden
                  accept={'image/*'}
                  onChange={handleFileUpload}
                />
              </Button>
            </Grid>
            <Grid item>
              {typeof files[0] !== 'string' ? (
                files.map((file, i) => {
                  return (
                    <Typography
                      overflow={'hidden'}
                      variant="caption"
                      component={'sub'}
                      sx={{ mb: 1 }}
                      key={i}
                    >
                      {file.name.length > 18
                        ? file.name.substring(0, 18) +
                          '... ' +
                          file.name.substring(file.name.lastIndexOf('.'))
                        : file.name}
                    </Typography>
                  )
                })
              ) : (
                <Typography variant="caption" component={'sub'}>
                  Files loaded...
                </Typography>
              )}
            </Grid>
          </Grid>
        </Grid>
      </Container>

      <Grid
        container
        columnSpacing={4}
        columns={20}
        direction="row"
        alignItems="center"
        justifyContent="center"
        sx={{ minWidth: "100%", mt: 17 }}
      >
        <Grid item xs={3}>
          <Button onClick={() => save(Save.PDF)} variant="contained">
            <PictureAsPdf />
            <Typography sx={{ ml: '4px' }}>Als PDF herunterladen</Typography>
          </Button>
        </Grid>
        <Grid item xs={3}>
          <Button onClick={() => save(Save.JSON)} variant="outlined">
            <Typography>Als JSON herunterladen</Typography>
          </Button>
        </Grid>
        <Grid item xs={2}>
          <Button onClick={() => save(Save.BROWSER)} variant="text">
            <BrowserUpdatedOutlined />
            <Typography>Im Browser speichern</Typography>
          </Button>
        </Grid>
      </Grid>
    </>
  )
}

export default ToolPage
